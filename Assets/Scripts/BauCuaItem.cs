﻿using UnityEngine;
using System;

[Serializable]
public class BauCuaItem {
   public int BAU=0;
   public int CUA=0;
   public int CA=0;
   public int TOM=0;
   public int GA=0;
   public int NAI=0;
}
