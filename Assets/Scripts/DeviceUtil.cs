﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class DeviceUtil
{
    public static string getDeviceId()
    {
#if UNITY_ANDROID
        return getDeviceIdAndroid();
#else
            return 
             SystemInfo.deviceUniqueIdentifier;
#endif
    }

    public static string getDeviceName()
    {
        return SystemInfo.deviceModel + " - " + SystemInfo.deviceName;
    }

    private static string getDeviceIdAndroid()
    {
        if (Application.isEditor)
        {
            return "Editor: " + SystemInfo.deviceUniqueIdentifier;
        }

#if UNITY_ANDROID
        AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = up.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject contentResolver = currentActivity.Call<AndroidJavaObject>("getContentResolver");
        AndroidJavaClass secure = new AndroidJavaClass("android.provider.Settings$Secure");
        string android_id = secure.CallStatic<string>("getString", contentResolver, "android_id");
        return android_id;
#endif

        return "Editor: " + SystemInfo.deviceUniqueIdentifier;
    }
}

