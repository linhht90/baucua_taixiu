﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;

public class Dice : MonoBehaviour
{
    private const float MaxAngular = 100f;
    private Rigidbody rib;
    private Vector3 startPoint;
    private Quaternion quaternionStart;

    [SerializeField]
    private GameObject[] graphics;

    private AudioSource audioSource;
    private void Awake()
    {
        rib = GetComponent<Rigidbody>();
        rib.useGravity = false;
        rib.maxAngularVelocity = MaxAngular;
        startPoint = transform.position;
        quaternionStart = transform.rotation;
        audioSource = GetComponent<AudioSource>();
    }

    private void Reset()
    {
        transform.position = startPoint;
        transform.rotation = quaternionStart;

        rib.angularVelocity = Vector3.zero;
        rib.velocity = Vector3.zero;

        rib.useGravity = false;
    }

    public Vector3 fallBauCua(BauCuaType type)
    {
        print("fall");

        Reset();
        rib.useGravity = true;

        Vector3 force = RandomUtils.randomVectorForRotate();
        if (type != BauCuaType.NONE)
        {
            force = FixData.dicBauCua[type];
        }


        Debug.Log(gameObject.name + " : " + force.ToString());

        // Make random effect
        float rndRotate = Random.Range(0, 360);
        force = Quaternion.AngleAxis(rndRotate, Vector3.down) * force;
        transform.Rotate(Vector3.down, rndRotate);
        rib.angularVelocity = force;

        return force;
    }

    public Vector3 fallTaiXiu(int number=-1)
    {
        print("fall");

        Reset();
        rib.useGravity = true;

        Vector3 force = RandomUtils.randomVectorForRotate();
        if (number != -1)
        {
            force = FixData.dicTaiXiu[number];
        }

        Debug.Log(gameObject.name + " : " + force.ToString());

        // Make random effect
        float rndRotate = Random.Range(0, 360);
        force = Quaternion.AngleAxis(rndRotate, Vector3.down) * force;
        transform.Rotate(Vector3.down, rndRotate);
        rib.angularVelocity = force;

        return force;
    }

    public void setGraphic(int indexGraphic)
    {
        foreach (GameObject g in graphics)
        {
            g.SetActive(false);
        }

        graphics[indexGraphic].SetActive(true);
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag.Equals("Dia"))
        {
            MusicController.Instane.playMusic_Sick();
        }
    }

    public bool isXocing()
    {
        if (rib.velocity != Vector3.zero || rib.angularVelocity != Vector3.zero)
        {
            return true;
        }
        return false;
    }
}
