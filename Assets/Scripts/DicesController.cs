﻿using UnityEngine;
using System.Collections;
using System;

public class DicesController : MonoBehaviour
{
    public static DicesController Instance;

    [SerializeField]
    private bool isCheat = false;
    private bool isBauCuaGame = true;

    private void Awake()
    {
        isCheat = true;
        Instance = this;
    }

    [SerializeField]
    private Dice[] dices;
    public void Xoc()
    {
        if(isXocing()){
            return;
        }

        MusicController.Instane.playMusic_Xoc();
        if (isBauCuaGame)
        {

            BauCuaItem bauCuaItem = ServerController.Instance.getBauCuaItem();
            if (isCheat && bauCuaItem != null)
            {
                BauCuaType[] types = new BauCuaType[3];
                int count = 0;

                for (int i = 0; i < bauCuaItem.TOM; i++)
                {
                    types[count++] = BauCuaType.TOM;
                }
                for (int i = 0; i < bauCuaItem.CUA; i++)
                {
                    types[count++] = BauCuaType.CUA;
                }
                for (int i = 0; i < bauCuaItem.CA; i++)
                {
                    types[count++] = BauCuaType.CA;
                }
                for (int i = 0; i < bauCuaItem.GA; i++)
                {
                    types[count++] = BauCuaType.GA;
                }
                for (int i = 0; i < bauCuaItem.BAU; i++)
                {
                    types[count++] = BauCuaType.BAU;
                }
                for (int i = 0; i < bauCuaItem.NAI; i++)
                {
                    types[count++] = BauCuaType.NAI;
                }

                XocBauCua(types[0], types[1], types[2]);
            }
            else
            {
                XocBauCua(BauCuaType.NONE, BauCuaType.NONE, BauCuaType.NONE);
            }
        }
        else
        {
            TaiXiuItem taiXiuItem = ServerController.Instance.getTaiXiuItem();
            if (isCheat && taiXiuItem!=null)
            {
                XocTaiXiu(taiXiuItem.MAT1, taiXiuItem.MAT2, taiXiuItem.MAT3);

            }
            else
            {
                XocTaiXiu(RANDOM, RANDOM, RANDOM);
            }
        }
    }
    private const int RANDOM = -1;
    private void XocBauCua(BauCuaType type1, BauCuaType type2, BauCuaType type3)
    {
        dices[0].fallBauCua(type1);
        dices[1].fallBauCua(type2);
        dices[2].fallBauCua(type3);
    }

    private void XocTaiXiu(int mat1, int mat2, int mat3)
    {
        dices[0].fallTaiXiu(mat1);
        dices[1].fallTaiXiu(mat2);
        dices[2].fallTaiXiu(mat3);
    }

    public void setStyle(int style, bool isBauCua)
    {
        isBauCuaGame = isBauCua;
        for (int i = 0; i < dices.Length; i++)
        {
            dices[i].setGraphic(style);
        }
    }

    public void OnOffCheat()
    {
        isCheat = !isCheat;
    }

    private bool isXocing(){
        foreach(Dice dice in dices){
            if(dice.isXocing()){
                return true;
            }
        }

        return false;
    }
}
