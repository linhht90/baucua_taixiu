﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public enum BauCuaType { NONE, TOM, CUA, CA, GA, BAU, NAI }
    class FixData
    {

        public static Vector3 ONE = new Vector3(14.7f, 17.7f, 13.7f);
        public static Vector3 TWO = new Vector3(11.7f, 12.3f, 12.6f);
        public static Vector3 THREE = new Vector3(16.6f, 17.8f, 19.3f);
        public static Vector3 FOUR = new Vector3(19.4f, 13.0f, 12.6f);
        public static Vector3 FIVE = new Vector3(18.4f, 19.5f, 17.2f);
        public static Vector3 SIX = new Vector3(16.8f, 13.0f, 13.7f);

        public static Dictionary<int, Vector3> dicTaiXiu = new Dictionary<int, Vector3>() { { 1, ONE }, { 2, TWO }, { 3, THREE }, { 4, FOUR }, { 5, FIVE }, { 6, SIX } };


        public static Vector3 TOM = new Vector3(16.2f, 11.0f, 18.7f);
        public static Vector3 CUA = new Vector3(10.7f, 11.3f, 18.3f);
        public static Vector3 CA = new Vector3(12.1f, 10.3f, 11.6f);
        public static Vector3 GA = new Vector3(11.1f, 17.1f, 16.0f);
        public static Vector3 BAU = new Vector3(11.0f, 15.3f, 17.5f);
        public static Vector3 NAI = new Vector3(16.7f, 12.5f, 10.9f);

        public static Dictionary<BauCuaType, Vector3> dicBauCua = new Dictionary<BauCuaType, Vector3>() { { BauCuaType.TOM, TOM }, { BauCuaType.CUA, CUA }, { BauCuaType.CA, CA }, { BauCuaType.GA, GA }, { BauCuaType.BAU, BAU }, { BauCuaType.NAI, NAI } };
    }

