﻿using UnityEngine;
using System.Collections;

public class MusicController : MonoBehaviour {

    [SerializeField]
    private AudioClip xocClip;
    [SerializeField]
    private AudioClip bauCuaClip;
    [SerializeField]
    private AudioClip taiXiuClip;
    [SerializeField]
    private AudioClip[] sickClips;

    private AudioSource player;
    public static MusicController Instane;

    private bool isMusic = true;
    private void Awake()
    {
        Instane = this;
        player = GetComponent<AudioSource>();
    }

    public void playMusic_Xoc()
    {
        player.PlayOneShot(xocClip);
    }

    public void  playMusic_BauCua(){
        player.PlayOneShot(bauCuaClip);
    }

    public void playMusic_TaiXiu()
    {
        player.PlayOneShot(bauCuaClip);
    }

    public void playMusic_Sick()
    {
        int index = Random.Range(0, sickClips.Length);

        player.PlayOneShot(sickClips[index]);
    }

    public bool OnOffMusic()
    {
        isMusic = !isMusic;
        if (isMusic)
        {
            player.volume = 1;
        }
        else
        {
            player.volume = 0;
        }

        return isMusic;
    }


}
