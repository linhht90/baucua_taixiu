﻿
using UnityEngine;

namespace Assets.Scripts
{
    class RandomUtils
    {
        public static Vector3 randomVectorForRotate()
        {
            float rndX = Random.Range(10f, 25f);
            float rndY = Random.Range(10f, 25f);
            float rndZ = Random.Range(10f, 25f);

            return new Vector3(rndX, rndY, rndZ);
        }
    }
}
