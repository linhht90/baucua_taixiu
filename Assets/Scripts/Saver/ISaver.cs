﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Saver
{
    public interface ISaver
    {

          void SaveJson(string Key, string content);
          string GetJson(string Key);
    }
}
