﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Saver
{
    class PlayerPrefsSaver : ISaver
    {
        private const string JSON_SAVER_KEY = "Json8801";
        public void SaveJson(string Key, string content)
        {
            PlayerPrefs.SetString(Key, content);
        }

        public string GetJson(string Key)
        {
            string result = PlayerPrefs.GetString(Key, "");
            return result;
        }
    }
}
