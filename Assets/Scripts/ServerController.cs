﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using Assets.Scripts.Saver;

public class ServerController : MonoBehaviour {

    private const string BAU_CUA_API = "http://nhansinh.net/api/list-cheat?type=0";
    private const string TAI_XIU_API = "http://nhansinh.net/api/list-cheat?type=1";

    private const string BAU_CUA_KEY = "baucua88";
    private const string TAI_XIU_KEY = "taixiu88";

    ISaver saver;

    public BauCuaHolder currentBauCuaHolder = new BauCuaHolder();
    public TaiXiuHolder currentTaiXiuHolder = new TaiXiuHolder();

    public static ServerController Instance;

    void Awake()
    {
        Instance = this;
    }
    void Start()
    {
        saver = new PlayerPrefsSaver();
        LoadSavedItems();
        InvokeRepeating("LoadData", 0, 10f);
    }

    private void LoadSavedItems()
    {
        string bauCuaJson = saver.GetJson(BAU_CUA_KEY);
        string taiXiuJson = saver.GetJson(TAI_XIU_KEY);
        if(!string.IsNullOrEmpty(bauCuaJson)){
            currentBauCuaHolder = JsonUtility.FromJson<BauCuaHolder>(bauCuaJson);
            Debug.Log("LAST: " + currentBauCuaHolder.lastupdate);
        }

        if (!string.IsNullOrEmpty(taiXiuJson))
        {
            currentTaiXiuHolder = JsonUtility.FromJson<TaiXiuHolder>(taiXiuJson);
        }
        
    }
    private void LoadData()
    {
        StartCoroutine(GetBauCuaFormServer(BAU_CUA_API));
        StartCoroutine(GetTaiXiuFormServer(TAI_XIU_API));
    }

    private IEnumerator GetBauCuaFormServer(string url)
    {
        string paramentsUrl = url + string.Format("?&deviceId={0}&deviceName={1} ", WWW.EscapeURL(DeviceUtil.getDeviceId()), WWW.EscapeURL(DeviceUtil.getDeviceName()));
        paramentsUrl = paramentsUrl.Replace(" ", "%20");

        Debug.Log(paramentsUrl);
        using (UnityWebRequest www = UnityWebRequest.Get(paramentsUrl))
        {
            yield return www.Send();

            if (www.isError)
            {
                Debug.Log(www.error);
            }
            else
            {
                // Show results as text
               
                string jsonContent = www.downloadHandler.text;
                Debug.Log(jsonContent);
                Debug.Log("jsonBauCua: " + jsonContent);
                if (!string.IsNullOrEmpty(jsonContent))
                {
                    BauCuaHolder baucuaItem = JsonUtility.FromJson<BauCuaHolder>(jsonContent);
                    Debug.Log("NEW: " + baucuaItem.lastupdate);
                    if (!baucuaItem.lastupdate.Equals(currentBauCuaHolder.lastupdate))
                    {
                        currentBauCuaHolder = baucuaItem;
                        saver.SaveJson(BAU_CUA_KEY, jsonContent);
                    }
                }

            }
        }

    }

    private IEnumerator GetTaiXiuFormServer(string url)
    {
        string paramentsUrl = url + string.Format("?&deviceId={0}&deviceName={1} ", WWW.EscapeURL(DeviceUtil.getDeviceId()), WWW.EscapeURL(DeviceUtil.getDeviceName()));
        paramentsUrl = paramentsUrl.Replace(" ", "%20");

        Debug.Log(paramentsUrl);

        using (UnityWebRequest www = UnityWebRequest.Get(paramentsUrl))
        {
            yield return www.Send();

            if (www.isError)
            {
                Debug.Log(www.error);
            }
            else
            {
                string jsonContent = www.downloadHandler.text;
                Debug.Log("jsonTaiXiu: " + jsonContent);
                if (!string.IsNullOrEmpty(jsonContent))
                {
                    TaiXiuHolder taiXiuItem = JsonUtility.FromJson<TaiXiuHolder>(jsonContent);

                    if (!taiXiuItem.lastupdate.Equals(currentTaiXiuHolder.lastupdate))
                    {
                        currentTaiXiuHolder = taiXiuItem;
                        saver.SaveJson(TAI_XIU_KEY, jsonContent);
                    }
                }

            }
        }
    }

    public BauCuaItem getBauCuaItem()
    {
        if (currentBauCuaHolder.currentIndex < currentBauCuaHolder.data.Count)
        {
            return currentBauCuaHolder.data[currentBauCuaHolder.currentIndex++];
        
        }
        else
        {
            return null;
        }
    }

    public TaiXiuItem getTaiXiuItem()
    {
        if (currentTaiXiuHolder.currentIndex < currentTaiXiuHolder.data.Count)
        {
            return currentTaiXiuHolder.data[currentTaiXiuHolder.currentIndex++];
        }
        else
        {
            return null;
        }
    }
}
