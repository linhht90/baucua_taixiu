﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    private const int BAU_CUA_STYLE_INDEX = 0;
    private const int TAI_XIU_STYLE_INDEX = 1;

    [SerializeField]
    private Outline outlineBauCua;
    [SerializeField]
    private Outline outlineTaiXiu;

    private Sprite[] backgrounds;
    [SerializeField]
    private SpriteRenderer backgroundSpriteRender;
    private int backgroundIndex = 0;

    private void Start()
    {
        loadBackgrounds();
        outlineBauCua.enabled = true;
        outlineTaiXiu.enabled = false;
        DicesController.Instance.setStyle(BAU_CUA_STYLE_INDEX, true);
    }

    public void selectBauCua()
    {
        MusicController.Instane.playMusic_BauCua();
        outlineBauCua.enabled = true;
        outlineTaiXiu.enabled = false;
        DicesController.Instance.setStyle(BAU_CUA_STYLE_INDEX, true);
    }

    public void setlectTaiXiu()
    {
        MusicController.Instane.playMusic_TaiXiu();
        outlineBauCua.enabled = false;
        outlineTaiXiu.enabled = true;
        DicesController.Instance.setStyle(TAI_XIU_STYLE_INDEX, false);
    }

    private void loadBackgrounds()
    {
        backgrounds = Resources.LoadAll<Sprite>("Ban"); 
    }

    public void changeBackground()
    {
        if (backgrounds == null || backgrounds.Length < 1)
        {
            throw new Exception("Background sprites not loaded!");
        }

        backgroundIndex = (backgroundIndex + 1) % backgrounds.Length;
        backgroundSpriteRender.sprite = backgrounds[backgroundIndex];
    }

    [SerializeField]
    private GameObject startScreen;
    public void showStartScreen(bool isShow)
    {
        startScreen.SetActive(isShow);
    }

    public void Xoc()
    {
        if (DicesController.Instance != null)
        {
            DicesController.Instance.Xoc();
        }
        
    }

    public void setOnOffCheat()
    {
        DicesController.Instance.OnOffCheat();
    }

    [SerializeField]
    private Sprite btnSoundOn;
    [SerializeField]
    private Sprite btnSoundOff;
    [SerializeField]
    private Image btnSoundImage;
    public void OnOffSound()
    {
        bool isSoundOn = MusicController.Instane.OnOffMusic(); 
        if(isSoundOn){
            
        btnSoundImage.sprite = btnSoundOn;
        }else{
            
        btnSoundImage.sprite = btnSoundOff;
        }
    }

}
